# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 12:37:54 2017

@author: {aiyengar,thens}@hawk.iit.edu
"""

# refer for usage https://pymotw.com/3/configparser
from configparser import ConfigParser

# to make tuples from the strings in ini file
from ast import literal_eval

from Img import Img
from MatchPoints import MatchPoints
from Ransac import Ransac
from Panorama import Panorama

class PanoramaConfig:
    """ Config details for the ImageStitcher
     Follows the ini file format
     sections: panaroma, ransac, misc
    """
    def __init__(self, configfile):
        """ constructor """
        self.read_config_file(configfile)
        self.set_configuration()
        
    def set_configuration(self):
        Img._text_color = self.config['misc']['image_text_color']
        Img._text_size = self.config['misc']['image_text_size']
        Img._text_pos = self.config['misc']['image_text_pos']
        Img._image_resize = self.config['panorama']['image_resize']
        Img._image_match_ratio = self.config['panorama']['image_match_ratio']
        Ransac._max_trials = self.config['ransac']['max_trials']
        Ransac._stop_probability = self.config['ransac']['stop_probability']    
        Ransac._residual_threshold = self.config['ransac']['residual_threshold']
        Panorama.MIN_MATCH_COUNT = self.config['panorama']['min_match_count']
        Panorama.AUTO_BLEND = self.config['panorama']['auto_blend']
        Panorama.RESIZE_IMAGE = self.config['panorama']['resize_image']
        Panorama.BLEND_RATIO = self.config['panorama']['blend_ratio']
        Panorama.BLEND_MASK = self.config['debug']['blend_mask']
        Panorama.MATCH_IMAGE = self.config['debug']['sift_image']
        Panorama.SIFT_IMAGE = self.config['debug']['match_image']
        Panorama.REORDER_IMAGES_IF_NEEDED = self.config['panorama']['reorder_images_if_needed']
        
        
    def read_config_file(self, configfile):
        """ read the ini file and parse the options"""
        config = {}
        config['panorama'] = {}
        config['homography'] = {}
        config['ransac'] = {}
        config['debug'] = {}
        config['misc'] = {}
        parser = ConfigParser()
        parser.read(configfile)
        # panaroma section
        config['panorama']['min_match_count'] = parser.getint('panorama', 'min_match_count')
        config['panorama']['image_match_ratio'] = parser.getfloat('panorama', 'image_match_ratio')
        config['panorama']['auto_blend'] = parser.getboolean('panorama', 'auto_blend')
        config['panorama']['blend_ratio'] = parser.getfloat('panorama', 'blend_ratio')
        config['panorama']['resize_image'] = parser.getboolean('panorama', 'resize_image')
        config['panorama']['image_resize'] = literal_eval(parser.get('panorama', 'image_resize'))
        config['panorama']['reorder_images_if_needed'] = parser.getboolean('panorama', 'reorder_images_if_needed')
        # homography
        config['homography']['algorithm'] = parser.getint('homography', 'algorithm')
        # ransac
        config['ransac']['max_trials'] = parser.getint('ransac', 'max_trials')
        config['ransac']['residual_threshold'] = parser.getfloat('ransac', 'residual_threshold')
        config['ransac']['stop_probability'] = parser.getfloat('ransac', 'stop_probability')
        # debug
        config['debug']['sift_image'] = parser.getboolean('debug', 'sift_image')
        config['debug']['match_image'] = parser.getboolean('debug', 'match_image')
        config['debug']['ransac_outlier'] = parser.getboolean('debug', 'ransac_outlier')
        config['debug']['blend_mask'] = parser.getboolean('debug', 'blend_mask')
        # misc
        config['misc']['image_text_color'] = literal_eval(parser.get('misc', 'image_text_color'))
        config['misc']['image_text_size'] = parser.getfloat('misc', 'image_text_size')
        config['misc']['image_text_pos'] = literal_eval(parser.get('misc', 'image_text_pos'))
        self.config = config
        #print (config)
    def get_option(self, section, option):
        """ get option from the config dict"""
        return self.config[section][option]
# Testing
#a = PanoramaConfig("panorama.ini")
