# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 12:37:54 2017

@author: {aiyengar,thens}@hawk.iit.edu
"""
import cv2
import numpy as np
import sys
import os
from matplotlib import pyplot as plt

# our Modules
from Img import Img
from MatchPoints import MatchPoints
from Ransac import Ransac
from image_graph import ImageGraph

# This class provides functionality for image stitching. The assumption is that
# the image list is provided in a left to right order. We use SIFT to find matching
# keypoints in the images. These keypoints are used to warp the images together to
# produce a panoramic view. We use opencv for the most part.
class Panorama:
  # constants to refer the method for computing homography
  SIFT_IMAGE = True
  MATCH_IMAGE = True
  MIN_MATCH_COUNT = 100
  AUTO_BLEND = True
  RESIZE_IMAGE = True
  BLEND_RATIO = 0.15
  BLEND_MASK = True
  OPENCV_SVD_OPENCV_RANSAC = 1
  OWN_SVD_OWN_RANSAC = 2
  OPENCV_SVD_OWN_RANSAC = 3
  ONLY_SVD = 4
  Algorithm = {}
  Algorithm[OPENCV_SVD_OPENCV_RANSAC] = { 
          'description': "OpenCV SVD and OpenCV RANSAC",
          'imagefile': "opencv_svd_opencv_ransac",
          }
  Algorithm[OWN_SVD_OWN_RANSAC] = { 
          'description': "Own SVD and Own RANSAC",
          'imagefile': "own_svd_own_ransac",
          }
  Algorithm[OPENCV_SVD_OWN_RANSAC] = { 
          'description': "OpenCV SVD and Own RANSAC",
          'imagefile': "opencv_svd_own_ransac",
          }
  Algorithm[ONLY_SVD] = { 
          'description': "Only SVD",
          'imagefile': "only_svd"
          }
  # Initialization function (ctor). Sets up member variables for sharing state
  # within an instance.
  def __init__(self):
    self.MAIN_WINDOW_TITLE = "Panorama Stitcher Main Window"
    cv2.startWindowThread()

    self.images = []
    self.output_image = None
    self.stats = {}

  # Called when we have a new image directory |directory| to process. Initiates a read of the images
  # in the directory. Creates the image lists and invokes the CreatePanorama() function which combines
  # the images together and creates a panoramic view.
  def OnNewImageDirectory(self, directory, homography_algorithm):
    self.images = []
    self.image_files = []
    self.output_image = None

    try:
      self.image_files = [os.path.join(directory, file_name) for file_name in os.listdir(directory) if os.path.isfile(os.path.join(directory, file_name))]
      if (Panorama.REORDER_IMAGES_IF_NEEDED):
        image_graph = ImageGraph(self.image_files)
        self.image_files = image_graph.GetImageList()

      self.images = [Img.imagefile(image_file,resize=self.RESIZE_IMAGE) for image_file in self.image_files]
      self.image_count = len(self.images)
      
      count = 0
      for image in self.images:
          count +=1
          cv2.imshow("Image"+str(count),image.image_data)

      # Add calls to CreatePanorama for every homography algorithm we support.
      #self.CreatePanorama(HomographyAlgorithm.OPEN_CV, 'opencv', self.MAIN_WINDOW_TITLE + " using OpenCV homography")
      #self.CreatePanorama(HomographyAlgorithm.PLAIN_SVD, 'ownsvd', self.MAIN_WINDOW_TITLE + " using Plain SVD")
      #self.CreatePanorama(HomographyAlgorithm.RANSAC_SVD_T3, 'ownsvd_ransac_t3', self.MAIN_WINDOW_TITLE + " using Plain SVD and RANSAC(T=3)")
      #self.CreatePanorama(HomographyAlgorithm.RANSAC_SVD_T1, 'ownsvd_ransac_t1', self.MAIN_WINDOW_TITLE + " using Plain SVD and RANSAC(T=1)")
      #self.CreatePanorama(Panorama.OWN_SVD_OWN_RANSAC, 'cvsvd_ransac_t1', self.MAIN_WINDOW_TITLE + " using CV SVD and RANSAC")

      self.CreatePanorama(homography_algorithm,Panorama.Algorithm[homography_algorithm]['imagefile'], self.MAIN_WINDOW_TITLE + Panorama.Algorithm[homography_algorithm]['description'])

      self.printStats()
      
    except OSError as err:
      print("Error in reading directory: ", directory)
      print("Please enter a valid directory")

  # Print summary statistics at the end
  def printStats(self):
      print ("%-20s%-4s%-8s%-8s%-8s%8s%20s" % ("Method", "Seq", "Feat1", "Feat2", "Matches", "Size", "Homography matrix (row major)"))

      for method in sorted(self.stats.iterkeys()):
          for sequence in sorted(self.stats[method].iterkeys()):
              print ("%-20s%d\t%d\t%d\t%d\t%10s %d\t%d\t%d\t, %d\t%d\t%d\t, %d\t%d\t%d" % 
                     (method,sequence,
                      self.stats[method][sequence]['feature1'],
                      self.stats[method][sequence]['feature2'],
                      self.stats[method][sequence]['matches'],
                      self.stats[method][sequence]['size'],
                      self.stats[method][sequence]['H'][0,0],
                      self.stats[method][sequence]['H'][0,1],
                      self.stats[method][sequence]['H'][0,2],
                      self.stats[method][sequence]['H'][1,0],
                      self.stats[method][sequence]['H'][1,1],
                      self.stats[method][sequence]['H'][1,2],
                      self.stats[method][sequence]['H'][2,0],
                      self.stats[method][sequence]['H'][2,1],
                      self.stats[method][sequence]['H'][2,2]))

  # Creates a panoramic view from the image lists.
  def CreatePanorama(self, homography_algorithm, name, window_title):
     
      # store cwd and create new directory
      curdir = os.getcwd()
      if not os.path.exists("./"+name): os.mkdir("./"+name)
      os.chdir("./"+name)

      self.stats[name] = {}
      pair = 0
      pan_count = 1
      temp_image = self.images[0].image_data
      for nextImage in self.images[1:]:
          pair += 1
          self.stats[name][pair] = {}
          currentImage = Img(image_data=temp_image,filename="pan"+str(pan_count))
          currentImage.getImageFeatures()
          currentImage.writeImageFeatures()
          nextImage.getImageFeatures()
          if Panorama.SIFT_IMAGE: nextImage.writeImageFeatures() 
          (pts_image1, pts_image2, match_count) = currentImage.matchImage(nextImage,writeMatchImage=Panorama.MATCH_IMAGE)

          # record all the details
          self.stats[name][pair]['feature1'] = len(currentImage.features)
          self.stats[name][pair]['feature2'] = len(nextImage.features)
          self.stats[name][pair]['matches']  = match_count
          self.stats[name][pair]['size']     = "NA"
          self.stats[name][pair]['H']        = np.zeros((3,3))

          if (pts_image1 is None or len(pts_image1) < Panorama.MIN_MATCH_COUNT):
              print("ERROR: Failed to match image\n")
              return #self.Exit()
          # check if we have have minimum matches
          homography, status = self.GetHomographyMatrix(pts_image1, pts_image2, homography_algorithm)
          if (homography is None):
              print("ERROR: Failed to match image\n")
              return #self.Exit()
                
          temp_image = self.WarpSourceWithDestination(nextImage.image_data, currentImage.image_data, homography)
          pan_count += 1
          # record remaining details
          self.stats[name][pair]['size']     = "%d x %d" % (temp_image.shape[1], temp_image.shape[1] )
          self.stats[name][pair]['H']        = np.round(homography)
          
      # one final sharpen 
      #kernel = np.array([ [-1, -1, -1], [-1, 9, -1], [-1, -1, -1] ]);
      #temp_image = cv2.filter2D(temp_image,cv2.CV_64F,kernel)
      self.output_image = Img(image_data=temp_image,filename=name+'final_panorama')
      cv2.imshow(window_title, self.output_image.image_data)
      cv2.imwrite("../"+name+'_panorama.jpg',self.output_image.image_data)
      os.chdir(curdir)

  # Warps the |source| image to |destination| using the |homography| matrix
  # passed in.
  def WarpSourceWithDestination(self, destination, source, homography):
    rows_destination, cols_destination = destination.shape[:2]
    rows_source, cols_source = source.shape[:2]

    destination_points = np.float32([[0, 0], [0, rows_destination],
        [cols_destination, rows_destination], [cols_destination, 0]]).reshape(-1, 1, 2)

    source_points = np.float32([[0, 0], [0, rows_source], [cols_source, rows_source],
        [cols_source, 0]]).reshape(-1, 1, 2)
    
    # Find the perspective transformation for the query image (image2), which will
    # be used to transform the corners of the image to corresponding points in the
    # training image (image1)
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html
    transformed_source_points = cv2.perspectiveTransform(source_points, homography)
    merged_points = np.concatenate((destination_points, transformed_source_points), axis=0)

    # Assuming that starting point of the image is (0, 0) and the ending point
    # is (end_row, end_col), the warped image starts from homography x [0, 0]
    # to homography x [end_row, end_col]. If the start point is -ve we need to
    # translate start by homography x [0, 0].

    # Compute the translation which needs to be applied to image2.
    [x_min, y_min] = np.int32(merged_points.min(axis=0).ravel() - 0.5)
    [x_max, y_max] = np.int32(merged_points.max(axis=0).ravel() + 0.5)

    # Apply the translation to the homography matrix
    translation_dist = [-x_min, -y_min]
    h_translation = np.array([[1, 0, translation_dist[0]], [0, 1, translation_dist[1]], [0, 0, 1]])

    result = cv2.warpPerspective(source, h_translation.dot(homography), (x_max - x_min, y_max - y_min))

    if Panorama.AUTO_BLEND:
        result = self.BlendImageWithTarget(result, destination, (translation_dist[0], translation_dist[1]))
    else :
        result[translation_dist[1]:rows_destination + translation_dist[1], translation_dist[0]:cols_destination + translation_dist[0]] = destination

    return result

  # Exits the program.
  def Exit(self):
    cv2.destroyAllWindows()
    sys.exit()

  # Returns the homography matrix for the points passed in. The matrix allows
  # for transforming |points1| into the space for |points2|. Currently uses the opencv
  # function to return the matrix. We will provide our own implementations for the
  # homography matrix here.
  def GetHomographyMatrix(self, points1, points2, homography_algorithm):
    if (homography_algorithm == Panorama.OPENCV_SVD_OPENCV_RANSAC):
      homography, status = cv2.findHomography(points1, points2, cv2.RANSAC, 3)
      print("\n OpenCV homography matrix is")
      print(homography)
      return (homography, status)
    elif (homography_algorithm == Panorama.ONLY_SVD):
      # Best effort to find a valid homography. We use a sliding window approach to find
      # the best homography. We assume a homography with a singular value smaller than 0.5
      # as a good one. Singular values greater than that give very bad results.
      sample_size = len(points1) / 5
      for i in range(0, len(points1) - sample_size):
        points1_slice = points1[i:sample_size]
        points2_slice = points2[i:sample_size]
        print ("INFO: sample_size =", sample_size)
        print ("INFO: len points1 =", len(points1))
        print ("INFO: i           =", i)
        print ("INFO: points1 sli =", len(points1_slice))
        
        homography, singular_value = MatchPoints.GetHomographyMatrixUsingSVD(points1_slice, points2_slice)
        if np.isnan(singular_value):
          continue
        if (singular_value <= 0.5):
          #print("Singular value: ", singular_value)
          #print("\n Plain SVD homography matrix")
          #print(homography)
          return (homography, 1)
        else:
          print("Continuing search for better homography matrix. Current Singular value: ", singular_value)
    elif (homography_algorithm == Panorama.OWN_SVD_OWN_RANSAC):
        MP = MatchPoints(points1,points2)
        r = Ransac(MP,use_own_svd=True)
        r.estimate()
        return (r.best['H'],1)
    elif (homography_algorithm == Panorama.OPENCV_SVD_OWN_RANSAC):
        MP = MatchPoints(points1,points2)
        r = Ransac(MP,use_own_svd=False)
        r.estimate()
        return (r.best['H'],1)
    else:
      print("Unsupported homography algorithm: ", homography_algorithm)
    return (None, -1)

  # Blends |image| with the |target| image. This is based on the seam reduction by
  # feathering technique. General idea is to assign a pixel to the image based
  # on the nearest center. 
  # Create a mask
  # Set mask(i, j) = 1 if pixel should come from the |image|.
  # Composite. imblend = im1_c * mask + im2_c * (1 - mask).
  # http://dhoiem.cs.illinois.edu/courses/vision_spring10/lectures/Lecture%2021%20-%20Photo%20Stitching.pdf
  # https://courses.engr.illinois.edu/cs498dwh/fa2010/lectures/Lecture%2017%20-%20Photo%20Stitching.pdf
  def BlendImageWithTarget(self, target, image, start_point,blend_ratio=None):
    if blend_ratio is None:
        blend_ratio = self.BLEND_RATIO
        
    x_start = start_point[0]
    y_start = start_point[1]

    blended_image = np.copy(target)
    blended_image[:,:,:] = 0

    # Generate a mask with the same size as the target. The values in the mask
    # are dependent on the proximity of the pixels to the |image|, which is being
    # blended with the target.
    mask = np.zeros((target.shape[0], target.shape[1]), 'int')
    maskf = np.zeros((target.shape[0], target.shape[1]), np.float)

    x_bounds = x_start + image.shape[1]
    y_bounds = y_start + image.shape[0]

    x_bounds = min(x_bounds, mask.shape[1])
    y_bounds = min(y_bounds, mask.shape[0])

    blend_window_width = blend_ratio * image.shape[1]

    for j in range (x_start, x_bounds):
        # Find the nearest edge.
        closest_edge = j - x_start + 1
        maskf[y_start:y_bounds,j] = closest_edge / blend_window_width
    
    # clamp values to 1
    maskf[maskf>1.0] = 1.0
    mask = cv2.convertScaleAbs(maskf, mask, 255, 0)
    
    print ("ASdasdasd", self.BLEND_MASK)
    if  self.BLEND_MASK:
        plt.imshow(mask)
        plt.title("Automatic Blending Mask (Ratio=%.2f)" % (blend_ratio))
        plt.show()
    # construct image1 such that image is contained in image1 
    image1 = np.stack(np.zeros((start_point[1], start_point[0],3),image.dtype))
    image1 = np.copy(blended_image)
    image1[start_point[1]:start_point[1]+image.shape[0],start_point[0]:start_point[0]+image.shape[1]] = image

    # simple addition will result in overflow, so need to do it in int16
    # and then convert back
    np.seterr(over='raise')
    np.seterr(under='raise')
    for i in range(3):
        temp = np.zeros(mask.shape,np.int16)
        temp = image1[:,:,i] * (mask / 255.0) + (1 - mask / 255.0) * target[:,:,i]
        temp[temp > 255] = 255
        blended_image[:,:,i] = temp

    return blended_image

  # Displays help for the program. Invoked via the 'h' key.
  def Help(self):
    print("\nPanorama Stitching for Static images\n")
    print("Usage: python image_stitcher.py <image directory>\n")
    print("Press ESC to exit")
    print("Press h for Help")
    print("Press i to enter a new image directory")

  # Invoked when the user wants to enter a new image directory for producing a panoramic
  # view.
  def GetImageDirectory(self):
    directory = raw_input('\nEnter image directory: ')
    self.OnNewImageDirectory(directory)

  # Processes the command identified by |key|. Supported commands today are ESC, 'h' and 'i'
  def ProcessCommand(self, key):
    if key == 27 or key == ord('q'):
      self.Exit()
    elif key == ord('h'):
      self.Help()
    elif key == ord('i'):
      self.GetImageDirectory()

  # Workhorse loop for processing user input.
  def RunLoop(self):
    while (True):
      key = cv2.waitKey(0)
      self.ProcessCommand(key)

