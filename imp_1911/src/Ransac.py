import numpy as np
      
# This class implements RANSAC algorithm
# N - total samples
# n - minimum samples for model fit
# t - tolerance threshold
# d - minimum consensus threshold
# k - number of iterations
# w - proportion of inliers in the dataset
class Ransac:
    # constants for this class
    _max_trials = 100
    _stop_probability = 0.999999
    _residual_threshold = 3
    def __init__(self,dataset,max_trials=None,stop_probability=None,residual_threshold=None,use_own_svd=False):
        
        self.dataset = dataset
        self.max_trials = max_trials or self._max_trials
        self.stop_probability = stop_probability or self._stop_probability
        self.residual_threshold = residual_threshold or self._residual_threshold
        
        self.use_own_svd = use_own_svd
        self.best = {}
        self.best['iter']  = 0
        self.best['n_in']  = 0
        self.best['n_out'] = 0
        self.best['H']     = 0
        self.best['score'] = 0
        
        self.best_inmask = 0 # this is different as we do not want to clutter
        self.book = {}
        
    def estimate(self):
        i = 0
        while (i< self.max_trials):
            rp1,rp2 = self.dataset.drawSamples()
            if self.use_own_svd:
                H,status = self.dataset.findHomography4OwnSVD(rp1,rp2)
            else:
                H,status = self.dataset.findHomography4CV(rp1,rp2)

            # check if H is valid
            if H is None:  continue
            n_in,n_out,w,score,inmask = self.dataset.computeInliers(H,self.residual_threshold)

            # if w is already 1, avoi divide by zero 
            if (np.abs(w-1) < 0.00001):
                k = i # we can stop here
            else:
                k = np.abs(np.log(1-self.stop_probability)/np.log(1-w**2)) # sometimes we get -inf
            
            # record details
            self.book[i] = {}
            self.book[i]['n_in']  = n_in
            self.book[i]['n_out'] = n_out
            self.book[i]['H']     = H
            self.book[i]['w']     = w
            self.book[i]['k']     = k
            self.book[i]['score'] = score
        
            if (self.best['n_in'] < n_in) or ( self.best['n_in'] == n_in and self.best['score'] < score):
                self.best['n_in']  = n_in
                self.best['n_out'] = n_out
                self.best['H']     = H
                self.best['iter']  = i
                self.best['score'] = score
                self.best_inmask   = inmask

            i+=1
            if (i>self.max_trials<i): break
            if (i>k): break
        # print statistics
        self.printStats()
        
    def printStats(self):
        for key in sorted(self.book.iterkeys()):
            print ("%d\t%.2f\t%d\t%d\t%.2f\t%.2f\t" % 
                   (key,
                   self.book[key]['score'],
                   self.book[key]['n_in'],
                   self.book[key]['n_out'],
                   self.book[key]['w'],
                   self.book[key]['k']))
        print (self.best)
