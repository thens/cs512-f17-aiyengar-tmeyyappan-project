import cv2
import numpy as np
import sys
import os
from operator import itemgetter

# some globals for annotating on top of the images
TEXT_COLOR = (255,255,255)
TEXT_SIZE  = 0.5
TEXT_POS   = (20,20)

# minimum match count for us to proceed
MINIMUM_MATCH = 100

# These enums specify the algorithm to be used for computing the homography matrix.
class HomographyAlgorithm:
  OPEN_CV = 1
  PLAIN_SVD = 2
  RANSAC_SVD_T3 = 3
  RANSAC_SVD_T1 = 4
  RANSAC_CVSVD_T1 = 5

# This class provides abstraction around an image object, will contain
#   image -> numpy image array
#   dirname
#   filename
#   extension
class Img:
    def __init__(self,image_data,resize=False,directory="./", filename="image1", fileext=".png"):
        # resize the image for speed
        if resize:
            image_data = cv2.resize(image_data, (400, 400))

        self.image_data = np.copy(image_data)
        self.directory = directory
        self.filename  = filename
        self.fileext   = fileext

    # using a class method so that we can create a Img object either from a file or 
    # from a numpy array. This is the pythonic way of doing multiple constructors
    @classmethod
    def imagefile(cls,image_file,resize=False):
        # read image and resize
        image_data = cv2.imread(image_file)
        if (image_data is None):
            print("ERROR: Failed to read image file: ", image_file)
            return image_data

        filewithoutext,fileext = os.path.splitext(image_file)
        directory = os.path.dirname(filewithoutext)
        filename  = os.path.basename(filewithoutext)

        return cls(image_data,resize=resize,filename=filename,fileext=fileext,directory=directory)
        
    
    # Write debug or annotated images corresponding to this image
    # we borrow the outdir, filename, extension from the parent image
    # output file name = outdir/<basename>-tag.<ext>
    def writeDebugImage(self,tag,image_data=None,outdir="./debug/"):
        if outdir is None: outdir=self.directory
        if os.path.exists(outdir) is False:
            os.makedirs(outdir)
        if image_data is None: image_data = self.image_data
        outfile = outdir+"/"+self.filename+"-"+tag+self.fileext
        cv2.imwrite(outfile,image_data)
        print ("INFO: Wrote ", outfile)


    # Returns features for the |image| using the opencv SIFT implementation.
    def getImageFeatures(self):
        sift_descriptor = cv2.xfeatures2d.SIFT_create()
        (keypoints, features) = sift_descriptor.detectAndCompute(self.image_data, None)

        self.keypoints = keypoints
        self.features  = features
        
    # annotate the keypoints on the image
    def writeImageFeatures(self):
        sift_image = np.copy(self.image_data)
        cv2.drawKeypoints(self.image_data,self.keypoints,sift_image,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        # add text on the image
        text = str(len(self.keypoints)) + " Features"
        cv2.putText(sift_image,text,TEXT_POS,cv2.FONT_HERSHEY_SIMPLEX, TEXT_SIZE,TEXT_COLOR)

        self.writeDebugImage('sift',image_data=sift_image)
        
    # Matches |image1| and |image2| using the opencv SIFT descriptor and returns the matching
    # keypoints on success. On failure returns empty keypoints.
    def matchImage(self, other, ratio=0.75):
        #Brute force matching with k nearest neighbors.  We take k = 2 to apply
        #ratio test. https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html
        matcher = cv2.BFMatcher()
        matches = matcher.knnMatch(self.features, other.features, k=2)
        
        # Apply ratio test
        good = []
        good_features = []
        for m, n in matches:
            if m.distance < ratio * n.distance:
                good_features.append((m.trainIdx, m.queryIdx, m.distance))
                good.append([m])
        good_features = sorted(good_features, key=itemgetter(2))

        # create a numpy array for showing the matching features
        #   width  = width of image1 + width of image2
        #   height = maximum of image1, image2 (we stack along the horizontal direction)
        h1,w1,c = self.image_data.shape
        h2,w2,c = other.image_data.shape
        match_image = np.zeros((max(h1,h2),w1+w2,c),self.image_data.dtype)
        cv2.drawMatchesKnn(self.image_data,self.keypoints,other.image_data,other.keypoints,good,match_image,flags=2)
        # add text on the image
        text = str(len(good)) + " Matches"
        cv2.putText(match_image,text,TEXT_POS,cv2.FONT_HERSHEY_SIMPLEX, TEXT_SIZE,TEXT_COLOR)
        self.writeDebugImage(other.filename+'-match', image_data=match_image)
        
        # we are in business if we have 4 matches
        if len(good_features) > 4:
            pts_image1 = np.float32([self.keypoints[i].pt for (_, i, _) in good_features])
            pts_image2 = np.float32([other.keypoints[i].pt for (i, _, _) in good_features])
            return (pts_image1, pts_image2)
        else:
            return (None, None)

# This class is required for RANSAC. RANSAC operates on this dataset.
# contains the set of points to be compared and the fit,score functions 
class MatchPoints:
    def __init__(self,p1,p2):
        # check if p1 and p2 is same length
        if len(p1) != len(p2):
            print ("ERROR: p1 and p2 not of same length")
            return
        self.p1 = p1
        self.p2 = p2
        self.N  = len(self.p1)
        
    def drawSamples(self):
        # get 4 random indices
        r = np.random.randint(0,self.N,4)
        #print ("INFO: Random", r)
        rp1 = np.zeros((4,2),self.p1.dtype)
        rp2 = np.zeros((4,2),self.p2.dtype)
        
        rp1[0,:] = self.p1[r[0],:]
        rp1[1,:] = self.p1[r[1],:]
        rp1[2,:] = self.p1[r[2],:]
        rp1[3,:] = self.p1[r[3],:]

        rp2[0,:] = self.p2[r[0],:]
        rp2[1,:] = self.p2[r[1],:]
        rp2[2,:] = self.p2[r[2],:]
        rp2[3,:] = self.p2[r[3],:]
        
        return rp1,rp2 

    # find a homography matrix for the 4 points that is passed using opencv
    def findHomography4CV(self,rp1, rp2):
        homography, status = cv2.findHomography(rp1, rp2, 0, 0)
        return homography,status

    # find a homography matrix for the 4 points that is passed using own SVD
    def findHomography4OwnSVD(self,rp1, rp2):
        homography, status = GetHomographyMatrixUsingSVD(rp1,rp2)
        return homography,status
    
    def computeInliers(self,H,t=3):
        # convert to homogenous and back
        # https://stackoverflow.com/questions/8486294/how-to-add-an-extra-column-to-an-numpy-array
        p1_h = np.zeros((self.p1.shape[0], self.p1.shape[1]+1),self.p1.dtype)
        p1_h[:,:-1]= self.p1
        p1_h[:,2] = 1.0
        # https://stackoverflow.com/questions/26289972/use-numpy-to-multiply-a-matrix-across-an-array-of-points
        dest = p1_h.dot(H.T)
        # convert back to non-homogenous co-ordinates
        dest1 = np.zeros(self.p1.shape,self.p1.dtype)
        dest1[:,0] = dest[:,0] / dest[:,2]
        dest1[:,1] = dest[:,1] / dest[:,2]
        
        # Compute residuals, inmask and score 
        #   remember x and y is flipped in the image array
        #   point is inlier if both X and Y are within the the threshold
        residuals_y = dest1[:,0] - self.p2[:,0]
        residuals_x = dest1[:,1] - self.p2[:,1]
        score       = np.sum(residuals_x**2 + residuals_y**2)
        inmask_x    = np.abs(residuals_x) < t
        inmask_y    = np.abs(residuals_y) < t
        inmask      = np.logical_and(inmask_x,inmask_y)
        
        # count inliners, w
        n_in  = np.sum(inmask)
        n_out = len(self.p1) - n_in
        w = float(n_in)/(n_in+n_out)
        return n_in, n_out, w, score, inmask
    
    # Lets summarize the H matrix computed from all methods
    # 1) opencv homography for all points without RANSAC
    # 2) opencv homography for all points with opencv RANSAC
    # 3) opencv homography4 computed by our own RANSAC
    # 4) opencv homography for all inlier points determined by our own RANSAC
    def summarizeMethods(self,ransac1_H,ransac1_inmask,ransac2_H,ransac2_inmask,t=3):
        
        hSummary = {}
        H_cv_R_none,s1    = cv2.findHomography(self.p1, self.p2,0)
        H_cv_R_cv,s2      = cv2.findHomography(self.p1, self.p2,cv2.RANSAC,t)
        H_cv4_R_own       = ransac1_H
        H_own_R_own       = ransac2_H
        H_cvmask_R_own,s4 = cv2.findHomography(self.p1[ransac1_inmask], self.p2[ransac1_inmask],0)

        hSummary['H_cv_R_none']    = {}
        hSummary['H_cv_R_cv']      = {}
        hSummary['H_cv4_R_own']    = {}
        hSummary['H_own_R_own']    = {}
        hSummary['H_cvmask_R_own'] = {}
        
        hSummary['H_cv_R_none']['H']    = H_cv_R_none
        hSummary['H_cv_R_cv']['H']      = H_cv_R_cv
        hSummary['H_cv4_R_own']['H']    = H_cv4_R_own
        hSummary['H_own_R_own']['H']    = ransac2_H
        hSummary['H_cvmask_R_own']['H'] = H_cvmask_R_own
        
        for method in hSummary.keys():
            H = hSummary[method]['H']
            n_in, n_out, w, score, inmask = self.computeInliers(H,t)
            hSummary[method]['score'] = score
            hSummary[method]['n_in']  = n_in
            hSummary[method]['n_out'] = n_out
            hSummary[method]['w']     = w
        
        # print the details
        print ("\n\nSummary:\nResidual Threshold = %.2f" % t)
        print ("%-20s\t%-10s\t%-10s\t%-10s\t" % ("Method", '#inliners', 'Score', '#outliers'))
        for method in hSummary.keys():
            print ("%-20s\t%d\t\t%.2f\t%d" % 
                   (method,
                    hSummary[method]['n_in'],
                    hSummary[method]['score'],
                    hSummary[method]['n_out']))
            
        
# This class implements RANSAC algorithm
# N - total samples
# n - minimum samples for model fit
# t - tolerance threshold
# d - minimum consensus threshold
# k - number of iterations
# w - proportion of inliers in the dataset
class Ransac:
    def __init__(self,dataset,max_trials=100,stop_probability=0.9999999999,residual_threshold=1,use_own_svd=False):
        
        self.dataset = dataset
        self.max_trials = max_trials
        self.stop_probability = stop_probability
        self.residual_threshold = residual_threshold
        self.use_own_svd = use_own_svd
        self.best = {}
        self.best['iter']  = 0
        self.best['n_in']  = 0
        self.best['n_out'] = 0
        self.best['H']     = 0
        self.best['score'] = 0
        
        self.best_inmask = 0 # this is different as we do not want to clutter
        self.book = {}
        
    def estimate(self):
        i = 0
        while (i< self.max_trials):
            rp1,rp2 = self.dataset.drawSamples()
            if self.use_own_svd:
                H,status = self.dataset.findHomography4OwnSVD(rp1,rp2)
            else:
                H,status = self.dataset.findHomography4CV(rp1,rp2)

            # check if H is valid
            if H is None:  continue
            n_in,n_out,w,score,inmask = self.dataset.computeInliers(H,self.residual_threshold)

            # if w is already 1, avoi divide by zero 
            if (np.abs(w-1) < 0.00001):
                k = i # we can stop here
            else:
                k = np.abs(np.log(1-self.stop_probability)/np.log(1-w**2)) # sometimes we get -inf
            
            # record details
            self.book[i] = {}
            self.book[i]['n_in']  = n_in
            self.book[i]['n_out'] = n_out
            self.book[i]['H']     = H
            self.book[i]['w']     = w
            self.book[i]['k']     = k
            self.book[i]['score'] = score
        
            if (self.best['n_in'] < n_in) or ( self.best['n_in'] == n_in and self.best['score'] < score):
                self.best['n_in']  = n_in
                self.best['n_out'] = n_out
                self.best['H']     = H
                self.best['iter']  = i
                self.best['score'] = score
                self.best_inmask   = inmask

            i+=1
            if (i>self.max_trials<i): break
            if (i>k): break
        # print statistics
        self.printStats()
        
    def printStats(self):
        for key in sorted(self.book.iterkeys()):
            print ("%d\t%.2f\t%d\t%d\t%.2f\t%.2f\t" % 
                   (key,
                   self.book[key]['score'],
                   self.book[key]['n_in'],
                   self.book[key]['n_out'],
                   self.book[key]['w'],
                   self.book[key]['k']))
        print (self.best)


# This class provides functionality for image stitching. The assumption is that
# the image list is provided in a left to right order. We use SIFT to find matching
# keypoints in the images. These keypoints are used to warp the images together to
# produce a panoramic view. We use opencv for the most part.
class ImageStitcher:
  # Initialization function (ctor). Sets up member variables for sharing state
  # within an instance.
  def __init__(self):
    self.MAIN_WINDOW_TITLE = "Panorama Stitcher Main Window"
    cv2.startWindowThread()

    self.images = []
    self.output_image = None
    self.stats = {}

    self.ProcessCommandLine()
  
  # Processes the command line of the program. The initial image directory is mandatory.
  # The user can pass in other image directories by pressing the 'i' key later on. Please
  # refer to the Help() function for details.
  def ProcessCommandLine(self):
    if len(sys.argv) < 2:
      print("Insufficient arguments. The usage is image_stitcher.py <Image Directory>. Please pass the image directory\n")
      self.Exit()

    self.OnNewImageDirectory(sys.argv[1])

  # Called when we have a new image directory |directory| to process. Initiates a read of the images
  # in the directory. Creates the image lists and invokes the CreatePanorama() function which combines
  # the images together and creates a panoramic view.
  def OnNewImageDirectory(self, directory):
    self.images = []
    self.image_files = []
    self.output_image = None

    try:
      self.image_files = [os.path.join(directory, file_name) for file_name in os.listdir(directory) if os.path.isfile(os.path.join(directory, file_name))]
      self.images = [Img.imagefile(image_file,resize=True) for image_file in self.image_files]
      self.image_count = len(self.images)
      
      count = 0
      for image in self.images:
          count +=1
          cv2.imshow("Image"+str(count),image.image_data)

      # Add calls to CreatePanorama for every homography algorithm we support.
      #self.CreatePanorama(HomographyAlgorithm.OPEN_CV, 'opencv', self.MAIN_WINDOW_TITLE + " using OpenCV homography")
      #self.CreatePanorama(HomographyAlgorithm.PLAIN_SVD, 'ownsvd', self.MAIN_WINDOW_TITLE + " using Plain SVD")
      self.CreatePanorama(HomographyAlgorithm.RANSAC_SVD_T3, 'ownsvd_ransac_t3', self.MAIN_WINDOW_TITLE + " using Plain SVD and RANSAC(T=3)")
      self.CreatePanorama(HomographyAlgorithm.RANSAC_SVD_T1, 'ownsvd_ransac_t1', self.MAIN_WINDOW_TITLE + " using Plain SVD and RANSAC(T=1)")
      self.CreatePanorama(HomographyAlgorithm.RANSAC_CVSVD_T1, 'cvsvd_ransac_t1', self.MAIN_WINDOW_TITLE + " using CV SVD and RANSAC(T=1)")

      self.printStats()
      
    except OSError as err:
      print("Error in reading directory: ", directory)
      print("Please enter a valid directory")

  # Print summary statistics at the end
  def printStats(self):
      print ("%-20s%-4s%-8s%-8s%-8s%8s%20s" % ("Method", "Seq", "Feat1", "Feat2", "Matches", "Size", "Homography matrix (row major)"))

      for method in sorted(self.stats.iterkeys()):
          for sequence in sorted(self.stats[method].iterkeys()):
              print ("%-20s%d\t%d\t%d\t%d\t%10s %d\t%d\t%d\t, %d\t%d\t%d\t, %d\t%d\t%d" % 
                     (method,sequence,
                      self.stats[method][sequence]['feature1'],
                      self.stats[method][sequence]['feature2'],
                      self.stats[method][sequence]['matches'],
                      self.stats[method][sequence]['size'],
                      self.stats[method][sequence]['H'][0,0],
                      self.stats[method][sequence]['H'][0,1],
                      self.stats[method][sequence]['H'][0,2],
                      self.stats[method][sequence]['H'][1,0],
                      self.stats[method][sequence]['H'][1,1],
                      self.stats[method][sequence]['H'][1,2],
                      self.stats[method][sequence]['H'][2,0],
                      self.stats[method][sequence]['H'][2,1],
                      self.stats[method][sequence]['H'][2,2]))

  # Creates a panoramic view from the image lists.
  def CreatePanorama(self, homography_algorithm, name, window_title):
     
      # store cwd and create new directory
      curdir = os.getcwd()
      if not os.path.exists("./"+name): os.mkdir("./"+name)
      os.chdir("./"+name)

      self.stats[name] = {}
      pair = 0
      pan_count = 1
      temp_image = self.images[0].image_data
      for nextImage in self.images[1:]:
          pair += 1
          self.stats[name][pair] = {}
          currentImage = Img(image_data=temp_image,filename="pan"+str(pan_count))
          currentImage.getImageFeatures()
          currentImage.writeImageFeatures()
          nextImage.getImageFeatures()
          nextImage.writeImageFeatures()
          (pts_image1, pts_image2) = currentImage.matchImage(nextImage)

          # record all the details
          self.stats[name][pair]['feature1'] = len(currentImage.features)
          self.stats[name][pair]['feature2'] = len(nextImage.features)
          self.stats[name][pair]['matches']  = len(pts_image1)
          self.stats[name][pair]['size']     = "NA"
          self.stats[name][pair]['H']        = np.zeros((3,3))

          if (pts_image1 is None or len(pts_image1) < MINIMUM_MATCH):
              print("ERROR: Failed to match image\n")
              return #self.Exit()
          # check if we have have minimum matches
          homography, status = self.GetHomographyMatrix(pts_image1, pts_image2, homography_algorithm)
          if (homography is None):
              print("ERROR: Failed to match image\n")
              return #self.Exit()
                
          temp_image = self.WarpSourceWithDestination(nextImage.image_data, currentImage.image_data, homography)
          pan_count += 1
          # record remaining details
          self.stats[name][pair]['size']     = "%d x %d" % (temp_image.shape[1], temp_image.shape[1] )
          self.stats[name][pair]['H']        = np.round(homography)
          
      self.output_image = Img(image_data=temp_image,filename=name+'final_panorama')
      cv2.imshow(window_title, self.output_image.image_data)
      cv2.imwrite("../"+name+'_panorama.jpg',self.output_image.image_data)
      os.chdir(curdir)

  # Warps the |source| image to |destination| using the |homography| matrix
  # passed in.
  def WarpSourceWithDestination(self, destination, source, homography):
    rows_destination, cols_destination = destination.shape[:2]
    rows_source, cols_source = source.shape[:2]

    destination_points = np.float32([[0, 0], [0, rows_destination],
        [cols_destination, rows_destination], [cols_destination, 0]]).reshape(-1, 1, 2)

    source_points = np.float32([[0, 0], [0, rows_source], [cols_source, rows_source],
        [cols_source, 0]]).reshape(-1, 1, 2)
    
    # Find the perspective transformation for the query image (image2), which will
    # be used to transform the corners of the image to corresponding points in the
    # training image (image1)
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html
    transformed_source_points = cv2.perspectiveTransform(source_points, homography)
    merged_points = np.concatenate((destination_points, transformed_source_points), axis=0)

    # Assuming that starting point of the image is (0, 0) and the ending point
    # is (end_row, end_col), the warped image starts from homography x [0, 0]
    # to homography x [end_row, end_col]. If the start point is -ve we need to
    # translate start by homography x [0, 0].

    # Compute the translation which needs to be applied to image2.
    [x_min, y_min] = np.int32(merged_points.min(axis=0).ravel() - 0.5)
    [x_max, y_max] = np.int32(merged_points.max(axis=0).ravel() + 0.5)

    # Apply the translation to the homography matrix
    translation_dist = [-x_min, -y_min]
    h_translation = np.array([[1, 0, translation_dist[0]], [0, 1, translation_dist[1]], [0, 0, 1]])

    result = cv2.warpPerspective(source, h_translation.dot(homography), (x_max - x_min, y_max - y_min))
    return self.BlendImageWithTarget(result, destination, (translation_dist[0], translation_dist[1]), 0.05)

  # Exits the program.
  def Exit(self):
    cv2.destroyAllWindows()
    sys.exit()

  # Returns the homography matrix for the points passed in. The matrix allows
  # for transforming |points1| into the space for |points2|. Currently uses the opencv
  # function to return the matrix. We will provide our own implementations for the
  # homography matrix here.
  def GetHomographyMatrix(self, points1, points2, homography_algorithm):
    if (homography_algorithm == HomographyAlgorithm.OPEN_CV):
      homography, status = cv2.findHomography(points1, points2, cv2.RANSAC, 3)
      print("\n OpenCV homography matrix is")
      print(homography)
      return (homography, status)
    elif (homography_algorithm == HomographyAlgorithm.PLAIN_SVD):
      # Best effort to find a valid homography. We use a sliding window approach to find
      # the best homography. We assume a homography with a singular value smaller than 0.5
      # as a good one. Singular values greater than that give very bad results.
      sample_size = 4
      for i in range(0, len(points1) - sample_size):
        points1_slice = points1[i:i+sample_size]
        points2_slice = points2[i:i+sample_size]
        print ("INFO: sample_size =", sample_size)
        print ("INFO: len points1 =", len(points1))
        print ("INFO: i           =", i)
        print ("INFO: points1 sli =", len(points1_slice))
        
        homography, singular_value = GetHomographyMatrixUsingSVD(points1_slice, points2_slice)
        if np.isnan(singular_value):
          continue
        if (singular_value <= 0.5):
          #print("Singular value: ", singular_value)
          #print("\n Plain SVD homography matrix")
          #print(homography)
          return (homography, 1)
        else:
          print("Continuing search for better homography matrix. Current Singular value: ", singular_value)
    elif (homography_algorithm == HomographyAlgorithm.RANSAC_SVD_T3):
        MP = MatchPoints(points1,points2)
        r = Ransac(MP,max_trials=10000,residual_threshold=3,use_own_svd=True)
        r.estimate()
        return (r.best['H'],1)
    elif (homography_algorithm == HomographyAlgorithm.RANSAC_SVD_T1):
        MP = MatchPoints(points1,points2)
        r = Ransac(MP,max_trials=10000,residual_threshold=1,use_own_svd=True)
        r.estimate()
        return (r.best['H'],1)
    elif (homography_algorithm == HomographyAlgorithm.RANSAC_CVSVD_T1):
        MP = MatchPoints(points1,points2)
        r = Ransac(MP,max_trials=10000,residual_threshold=1,use_own_svd=False)
        r.estimate()
        return (r.best['H'],1)
    else:
      print("Unsupported homography algorithm: ", homography_algorithm)
    return (None, -1)

  # Blends |image| with the |target| image. This is based on the seam reduction by
  # feathering technique. General idea is to assign a pixel to the image based
  # on the nearest center. 
  # Create a mask
  # Set mask(i, j) = 1 if pixel should come from the |image|.
  # Composite. imblend = im1_c * mask + im2_c * (1 - mask).
  # http://dhoiem.cs.illinois.edu/courses/vision_spring10/lectures/Lecture%2021%20-%20Photo%20Stitching.pdf
  # https://courses.engr.illinois.edu/cs498dwh/fa2010/lectures/Lecture%2017%20-%20Photo%20Stitching.pdf
  def BlendImageWithTarget(self, target, image, start_point, blend_ratio = 0.1):
    x_start = start_point[0]
    y_start = start_point[1]

    blended_image = np.copy(target)

    # Generate a mask with the same size as the target. The values in the mask
    # are dependent on the proximity of the pixels to the |image|, which is being
    # blended with the target.
    mask = np.zeros((target.shape[0], target.shape[1]), 'int')

    x_bounds = x_start + image.shape[1]
    y_bounds = y_start + image.shape[0]

    x_bounds = min(x_bounds, mask.shape[1])
    y_bounds = min(y_bounds, mask.shape[0])

    blend_window_width = blend_ratio * image.shape[1]

    for i in range (y_start, y_bounds):
      for j in range (x_start, x_bounds):
        # Find the nearest edge.
        closest_edge = j - x_start + 1
        # The intensity is higher, the closer the edge is. We can think of this
        # like a probability value.
        intensity = min(1.0, closest_edge / blend_window_width)
        # Normalize to range between 0 and 255.
        intensity = intensity * 255
        mask[i, j] = intensity

    for i in range (0, mask.shape[0]):
      for j in range (0, mask.shape[1]):
        # Black = target.
        if (mask[i, j] == 0):
          blended_image[i, j] = target[i, j]
        # Black target = copy from image.
        elif (mask[i, j] > 0 and target[i, j][0] <= 10 and target[i, j][1] <= 10 and target[i, j][2] <= 10):
          blended_image[i, j] = image[i - start_point[1], j - start_point[0]]
        else:
          # We have an overlap with the image and the target. The final pixel value is dependent on whether it
          # is closer to the target(warped image) or to the |image| which needs to be blended.
          blended_image[i, j] = mask[i, j] / 255.0 * image[i - start_point[1], j - start_point[0]] + (1 - mask[i, j] / 255.0) * target[i,j]

    return blended_image

  # Displays help for the program. Invoked via the 'h' key.
  def Help(self):
    print("\nPanorama Stitching for Static images\n")
    print("Usage: python image_stitcher.py <image directory>\n")
    print("Press ESC to exit")
    print("Press h for Help")
    print("Press i to enter a new image directory")

  # Invoked when the user wants to enter a new image directory for producing a panoramic
  # view.
  def GetImageDirectory(self):
    directory = raw_input('\nEnter image directory: ')
    self.OnNewImageDirectory(directory)

  # Processes the command identified by |key|. Supported commands today are ESC, 'h' and 'i'
  def ProcessCommand(self, key):
    if key == 27 or key == ord('q'):
      self.Exit()
    elif key == ord('h'):
      self.Help()
    elif key == ord('i'):
      self.GetImageDirectory()

  # Workhorse loop for processing user input.
  def RunLoop(self):
    while (True):
      key = cv2.waitKey(0)
      self.ProcessCommand(key)

# Please refer to http://www.uio.no/studier/emner/matnat/its/UNIK4690/v16/forelesninger/lecture_4_3-estimating-homographies-from-feature-correspondences.pdf
# for information about normalization for DLT.
# We translate the centroid of the points to the origin and scale them such that the average distance of
# the points from the origin is sqrt(2)
def NormalizePointsForDLT(points):
  # We normalize the points using a matrix as below:
  # matrix = s x [1  0  -x]
  #              [0  0  -y]
  #              [0  0  -s]
  tx = np.average(points[:,0:1])
  ty = np.average(points[:,1:])

  scale = 0.0
  for i in range(0, len(points)):
    scale += np.sqrt((points[i, 0] - tx) * (points[i, 0] - tx) + (points[i, 1] - ty) * (points[i, 1] - ty))

  if scale == 0:
    scale = np.finfo(float).eps

  scale = np.sqrt(2.0) * len(points) / scale

  normalization_matrix = np.array([[scale, 0, -scale * tx], [0, scale, - scale * ty], [0, 0, 1]])

  homogeneous_points = np.append(points, np.ones([len(points), 1]), 1)

  for i in range(0, len(homogeneous_points)):
    homogeneous_points[i] = np.dot(normalization_matrix, np.transpose(homogeneous_points[i]))

  return (homogeneous_points, normalization_matrix)

# Please refer to http://6.869.csail.mit.edu/fa12/lectures/lecture13ransac/lecture13ransac.pdf
# http://vhosts.eecs.umich.edu/vision//teaching/EECS442_2011/lectures/discussion2.pdf
# http://www.cs.cmu.edu/~16385/spring15/lectures/Lecture15.pdf for information about the matrix
# A below. We try to solve an equation of the form A.H = 0 where H is the desired homography
# matrix.
def GetHomographyMatrixUsingSVD(points1, points2):
  points1_normalized, normalization_matrix1 = NormalizePointsForDLT(points1)
  points2_normalized, normalization_matrix2 = NormalizePointsForDLT(points2)

  A = []
  total_points = len(points1)

  for point_index in range(0, total_points):
    x1 = points1_normalized[point_index, 0]
    y1 = points1_normalized[point_index, 1]
    x2 = points2_normalized[point_index, 0]
    y2 = points2_normalized[point_index, 1]

    A.append([x1, y1, 1, 0, 0, 0, -x1*x2, -x2*y1, -x2])
    A.append([0, 0, 0, x1, y1, 1, -x1 * y2, -y1 * y2, -y2])

  U, D, V = np.linalg.svd(np.asarray(A), True)
  smallest_value_index = np.argmin(D)

  L = V[-1,:] / V[-1, -1]

  homography = L.reshape(3, 3)
  homography = np.dot(np.dot(np.linalg.inv(normalization_matrix2), homography), normalization_matrix1)
  return (homography, D[smallest_value_index])

#a = Image('C:/Users/thens/Documents/Personal/Master/Course/CS512/Assignments/Project/impl/data/list1/hill1.jpg')
#b = Image('C:/Users/thens/Documents/Personal/Master/Course/CS512/Assignments/Project/impl/data/list1/hill2.jpg')
#a.getImageFeatures(); b.getImageFeatures()
#a.matchImage(b)
image_stitcher = ImageStitcher()
image_stitcher.RunLoop()