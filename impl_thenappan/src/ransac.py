# -*- coding: utf-8 -*-
"""
Created on Sun Nov 05 09:23:26 2017

@author: thens
"""
import numpy as np
from sklearn import linear_model,datasets
from matplotlib import pyplot as plt

## Get current size
#fig_size = plt.rcParams["figure.figsize"]
#  
#fig_size[0] *= 1.5
#fig_size[1] *= 1.5
#plt.rcParams["figure.figsize"] = fig_size

N = 100
n_outliers = 10
t = 0.1

class LineData:
    def __init__(self,X=None,Y=None):
        
        # generate random data if not given
        if X is None:
            self.generateRandomData(N)
        else:
            self.X = X
            self.Y = Y
        
        self.N = self.X.shape[0]
        self.n = 2 # for line fitting

    def generateRandomData(self,N):
        np.random.seed(1)
        X, Y, coef = datasets.make_regression(n_samples=N, n_features=1,
                                      n_informative=1, noise=1,
                                      coef=True, random_state=1)
        self.X = X
        self.Y = Y.reshape(Y.shape[0],1)

        # Add outlier data
        np.random.seed(0)
        self.X[:n_outliers] = 3 + 0.5 * np.random.normal(size=(n_outliers, 1))
        self.Y[:n_outliers] = -3 + 10 * np.random.normal(size=(n_outliers, 1))

    def fitLine(self,points):
        x1 = points[0]
        y1 = points[1]
        x2 = points[2]
        y2 = points[3]
        m = (y2-y1)/(x2-x1)
        c = y1 - m*x1
        return (m,c)
        
    def drawSamples(self):
        r = np.random.randint(0,self.N,2)
        #print ("INFO: Random", r)
        return (self.X[r[0]], self.Y[r[0]], self.X[r[1]], self.Y[r[1]]) 

    def computeInliers(self,m,c,t):
        Yc = self.X*m + c
        residuals = Yc - self.Y
        inmask  = np.abs(residuals) < t
        n_in  = np.sum(inmask)
        n_out = self.N - n_in
        w = float(n_in)/(n_in+n_out)
        score = np.sum(residuals)
        return n_in, n_out, w, inmask, score
    
    def printData(self):
        print (self.X)
        print (self.Y)

    def plotData(self):
        plt.scatter(self.X,self.Y)
        
    def plotFit(self,m,c,t):
        n_in,n_out,w,inmask,score = self.computeInliers(m,c,t)
        outmask = np.logical_not(inmask)

        # Fit line using all data
        lr = linear_model.LinearRegression()
        lr.fit(self.X, self.Y)

        # call native ransac
        ransac = linear_model.RANSACRegressor(residual_threshold=t, max_trials=1000)
        ransac.fit(self.X, self.Y)
        print ("INFO: n_trials_ =", ransac.n_trials_)
        print ("INFO: n_skips_no_inliers_ =", ransac.n_skips_no_inliers_)
        print ("INFO: n_skips_invalid_data_ =", ransac.n_skips_invalid_data_)
        print ("INFO: n_skips_invalid_model_ =", ransac.n_skips_invalid_model_)

        # Predict data of estimated models
        line_X = np.arange(self.X.min(), self.X.max())[:, np.newaxis]
        line_Y = lr.predict(line_X)
        line_Y_ransac = ransac.predict(line_X)
        line_Y_me = m*line_X + c
        
        lw = 2
        plt.scatter(self.X[inmask], self.Y[inmask], color='yellowgreen', marker='.',
            label='Inliers')
        plt.scatter(self.X[outmask], self.Y[outmask], color='gold', marker='x',
            label='Outliers')
        plt.plot(line_X, line_Y, color='navy', linewidth=lw, label='Linear regressor')
        plt.plot(line_X, line_Y_ransac, color='cornflowerblue', linewidth=lw,
         label='RANSAC regressor')
        plt.plot(line_X, line_Y_me, color='magenta', linewidth=lw,
         label='My regressor')
        plt.legend(loc='lower right')
        plt.xlabel("Input")
        plt.ylabel("Response")
        plt.show()


        
# N - total samples
# n - minimum samples for model fit
# t - tolerance threshold
# d - minimum consensus threshold
# k - number of iterations
# w - proportion of inliers in the dataset
class Ransac:
    def __init__(self,dataset,max_trials=100,stop_probability=0.9999999999,residual_threshold=1):
        
        self.dataset = dataset
        self.max_trials = max_trials
        self.stop_probability = stop_probability
        self.residual_threshold = residual_threshold
        
        self.best = {}
        self.best['iter']  = 0
        self.best['n_in']  = 0
        self.best['n_out'] = 0
        self.best['m']     = 0
        self.best['c']     = 0
        self.best['score'] = 0
        
        self.book = {}
        
    def estimate(self):
        i = 0
        while (i< self.max_trials):
            m,c = self.dataset.fitLine(self.dataset.drawSamples())
            n_in,n_out,w,inmask,score = self.dataset.computeInliers(m,c,self.residual_threshold)

            # if w is already 1, avoi divide by zero 
            if (np.abs(w-1) < 0.00001):
                k = i # we can stop here
            else:
                k = np.abs(np.log(1-self.stop_probability)/np.log(1-w**2)) # sometimes we get -inf
            
            # record details
            self.book[i] = {}
            self.book[i]['n_in']  = n_in
            self.book[i]['n_out'] = n_out
            self.book[i]['m']     = m
            self.book[i]['c']     = c
            self.book[i]['w']     = w
            self.book[i]['k']     = k
            self.book[i]['score'] = score
        
            if (self.best['n_in'] < n_in) or ( self.best['n_in'] == n_in and self.best['score'] < score):
                self.best['n_in']  = n_in
                self.best['n_out'] = n_out
                self.best['m']     = m
                self.best['c']     = c
                self.best['iter']  = i
                self.best['score'] = score

            i+=1
            if (i>self.max_trials<i): break
            if (i>k): break
        # print statistics
        self.printStats()
        
    def printStats(self):
        for key in sorted(self.book.iterkeys()):
            print ("%d\t%f\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f\t" % 
                   (key,
                   self.book[key]['score'],
                   self.book[key]['n_in'],
                   self.book[key]['n_out'],
                   self.book[key]['w'],
                   self.book[key]['k'],
                   self.book[key]['m'],
                   self.book[key]['c']))
        print (self.best)

l = LineData()
#l.printData()
l.plotData()
m,c=l.fitLine(l.drawSamples())
#print(l.computeInliers(m,c,20))

r = Ransac(l,max_trials=10000,residual_threshold=5)
r.estimate()
l.plotFit(r.best['m'], r.best['c'],5)