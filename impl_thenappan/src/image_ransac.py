# -*- coding: utf-8 -*-
"""
Created on Sun Nov 05 21:38:02 2017

@author: thens
"""

import cv2
import numpy as np
import sys
import os
from scipy import ndimage
import math
from operator import itemgetter

# some globals
TEXT_COLOR = (255,255,255)
TEXT_SIZE  = 0.5
TEXT_POS   = (20,20)

# These enums specify the algorithm to be used for computing the homography matrix.
class HomographyAlgorithm:
  OPEN_CV = 1
  PLAIN_SVD = 2
  RANSAC_SVD = 3

# This class provides abstraction around an image object, will contain
#   image -> numpy image array
#   dirname
#   filename
#   extension
class Img:
    def __init__(self,image_data,resize=False,directory="./", filename="image1", fileext=".png"):
        # resize the image for speed
        if resize:
            image_data = cv2.resize(image_data, (400, 400))

        self.image_data = np.copy(image_data)
        self.directory = directory
        self.filename  = filename
        self.fileext   = fileext

    # using a class method so that we can create a Img object either from a file or 
    # from a numpy array
    @classmethod
    def imagefile(cls,image_file,resize=False):
        # read image and resize
        image_data = cv2.imread(image_file)
        if (image_data is None):
            print("ERROR: Failed to read image file: ", image_file)
            return image_data

        filewithoutext,fileext = os.path.splitext(image_file)
        directory = os.path.dirname(filewithoutext)
        filename  = os.path.basename(filewithoutext)

        return cls(image_data,resize=resize,filename=filename,fileext=fileext,directory=directory)
        
    
    # Write debug or annotated images corresponding to this image
    # we borrow the outdir, filename, extension from the parent image
    # output file name = outdir/<basename>-tag.<ext>
    def writeDebugImage(self,tag,image_data=None,outdir="./debug/"):
        if outdir is None: outdir=self.directory
        if os.path.exists(outdir) is False:
            os.makedirs(outdir)
        if image_data is None: image_data = self.image_data
        outfile = outdir+"/"+self.filename+"-"+tag+self.fileext
        cv2.imwrite(outfile,image_data)
        print ("INFO: Wrote ", outfile)


    # Returns features for the |image| using the opencv SIFT implementation.
    def getImageFeatures(self):
        sift_descriptor = cv2.xfeatures2d.SIFT_create()
        (keypoints, features) = sift_descriptor.detectAndCompute(self.image_data, None)

        self.keypoints = keypoints
        self.features  = features
    
    # annotate the keypoints on the image
    def writeImageFeatures(self):
        sift_image = np.copy(self.image_data)
        cv2.drawKeypoints(self.image_data,self.keypoints,sift_image,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        # add text on the image
        text = str(len(self.keypoints)) + " Features"
        cv2.putText(sift_image,text,TEXT_POS,cv2.FONT_HERSHEY_SIMPLEX, TEXT_SIZE,TEXT_COLOR)

        self.writeDebugImage('sift',image_data=sift_image)
        
    # Matches |image1| and |image2| using the opencv SIFT descriptor and returns the matching
    # keypoints on success. On failure returns empty keypoints.
    def matchImage(self, other, ratio=0.75):
        #Brute force matching with k nearest neighbors.  We take k = 2 to apply
        #ratio test. https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html
        matcher = cv2.BFMatcher()
        matches = matcher.knnMatch(self.features, other.features, k=2)
        
        # Apply ratio test
        good = []
        good_features = []
        for m, n in matches:
            if m.distance < ratio * n.distance:
                good_features.append((m.trainIdx, m.queryIdx, m.distance))
                good.append([m])
        good_features = sorted(good_features, key=itemgetter(2))

        # create a numpy array for showing the matching features
        #   width  = width of image1 + width of image2
        #   height = maximum of image1, image2 (we stack along the horizontal direction)
        h1,w1,c = self.image_data.shape
        h2,w2,c = other.image_data.shape
        match_image = np.zeros((max(h1,h2),w1+w2,c),self.image_data.dtype)
        cv2.drawMatchesKnn(self.image_data,self.keypoints,other.image_data,other.keypoints,good,match_image,flags=2)
        # add text on the image
        text = str(len(good)) + " Matches"
        cv2.putText(match_image,text,TEXT_POS,cv2.FONT_HERSHEY_SIMPLEX, TEXT_SIZE,TEXT_COLOR)
        self.writeDebugImage(other.filename+'-match', image_data=match_image)
        
        # we are in business if we have 4 matches
        if len(good_features) > 4:
            pts_image1 = np.float32([self.keypoints[i].pt for (_, i, _) in good_features])
            pts_image2 = np.float32([other.keypoints[i].pt for (i, _, _) in good_features])
            return (pts_image1, pts_image2)
        else:
            return (None, None)

class MatchPoints:
    def __init__(self,p1,p2):
        # check if p1 and p2 is same length
        if len(p1) != len(p2):
            print ("ERROR: p1 and p2 not of same length")
            return
        self.p1 = p1
        self.p2 = p2
        self.N  = len(self.p1)
        
    def drawSamples(self):
        # get 4 random indices
        r = np.random.randint(0,self.N,4)
        #print ("INFO: Random", r)
        rp1 = np.zeros((4,2),self.p1.dtype)
        rp2 = np.zeros((4,2),self.p2.dtype)
        
        rp1[0,:] = self.p1[r[0],:]
        rp1[1,:] = self.p1[r[1],:]
        rp1[2,:] = self.p1[r[2],:]
        rp1[3,:] = self.p1[r[3],:]

        rp2[0,:] = self.p2[r[0],:]
        rp2[1,:] = self.p2[r[1],:]
        rp2[2,:] = self.p2[r[2],:]
        rp2[3,:] = self.p2[r[3],:]
        
        return rp1,rp2 

    # find a homography matrix for the 4 points that is passed using opencv
    def findHomography4CV(self,rp1, rp2):
        homography, status = cv2.findHomography(rp1, rp2, 0, 0)
        return homography,status

    # find a homography matrix for the 4 points that is passed using own SVD
    def findHomography4OwnSVD(self,rp1, rp2):
        homography, status = GetHomographyMatrixUsingSVD(rp2,rp1)
        return homography,status
    
    def computeInliers(self,H,t=3):
        # convert to homogenous and back
        # https://stackoverflow.com/questions/8486294/how-to-add-an-extra-column-to-an-numpy-array
        p1_h = np.zeros((self.p1.shape[0], self.p1.shape[1]+1),self.p1.dtype)
        p1_h[:,:-1]= self.p1
        p1_h[:,2] = 1.0
        
        # https://stackoverflow.com/questions/26289972/use-numpy-to-multiply-a-matrix-across-an-array-of-points
        dest = p1_h.dot(H.T)
        
        # convert back to non-homogenous co-ordinates
        dest1 = np.zeros(p1.shape,p1.dtype)
        dest1[:,0] = dest[:,0] / dest[:,2]
        dest1[:,1] = dest[:,1] / dest[:,2]
        
        # Compute residuals, inmask and score 
        #   remember x and y is flipped in the image array
        #   point is inlier if both X and Y are within the the threshold
        residuals_y = dest1[:,0] - self.p2[:,0]
        residuals_x = dest1[:,1] - self.p2[:,1]
        score       = np.sum(residuals_x**2 + residuals_y**2)
        inmask_x    = np.abs(residuals_x) < t
        inmask_y    = np.abs(residuals_y) < t
        inmask      = np.logical_and(inmask_x,inmask_y)
        
        # count inliners, w
        n_in  = np.sum(inmask)
        n_out = len(p1) - n_in
        w = float(n_in)/(n_in+n_out)
        return n_in, n_out, w, score, inmask
    
    # Lets summarize the H matrix computed from all methods
    # 1) opencv homography for all points without RANSAC
    # 2) opencv homography for all points with opencv RANSAC
    # 3) opencv homography4 computed by our own RANSAC
    # 4) opencv homography for all inlier points determined by our own RANSAC
    def summarizeMethods(self,ransac_H,inmask,t=3):
        
        hSummary = {}
        H_cv_R_none,s1    = cv2.findHomography(self.p1, self.p2,0)
        H_cv_R_cv,s2      = cv2.findHomography(self.p1, self.p2,cv2.RANSAC,t)
        H_cv4_R_own       = ransac_H
        H_cvmask_R_own,s4 = cv2.findHomography(self.p1[inmask], self.p2[inmask],0)

        hSummary['H_cv_R_none']    = {}
        hSummary['H_cv_R_cv']      = {}
        hSummary['H_cv4_R_own']    = {}
        hSummary['H_cvmask_R_own'] = {}
        
        hSummary['H_cv_R_none']['H']    = H_cv_R_none
        hSummary['H_cv_R_cv']['H']      = H_cv_R_cv
        hSummary['H_cv4_R_own']['H']    = H_cv4_R_own
        hSummary['H_cvmask_R_own']['H'] = H_cvmask_R_own
        
        for method in hSummary.keys():
            H = hSummary[method]['H']
            n_in, n_out, w, score, inmask = self.computeInliers(H,t)
            hSummary[method]['score'] = score
            hSummary[method]['n_in']  = n_in
            hSummary[method]['n_out'] = n_out
            hSummary[method]['w']     = w
        
        # print the details
        print ("\n\nSummary:\nResidual Threshold = %.2f" % t)
        print ("%-20s\t%-10s\t%-10s\t%-10s\t" % ("Method", '#inliners', 'Score', '#outliers'))
        for method in hSummary.keys():
            print ("%-20s\t%d\t\t%.2f\t%d" % 
                   (method,
                    hSummary[method]['n_in'],
                    hSummary[method]['score'],
                    hSummary[method]['n_out']))
            
# N - total samples
# n - minimum samples for model fit
# t - tolerance threshold
# d - minimum consensus threshold
# k - number of iterations
# w - proportion of inliers in the dataset
class Ransac:
    def __init__(self,dataset,max_trials=100,stop_probability=0.9999999999,residual_threshold=1,use_own_svd=False):
        
        self.dataset = dataset
        self.max_trials = max_trials
        self.stop_probability = stop_probability
        self.residual_threshold = residual_threshold
        self.use_own_svd = use_own_svd
        self.best = {}
        self.best['iter']  = 0
        self.best['n_in']  = 0
        self.best['n_out'] = 0
        self.best['H']     = 0
        self.best['score'] = 0
        
        self.best_inmask = 0 # this is different as we do not want to clutter
        self.book = {}
        
    def estimate(self):
        i = 0
        while (i< self.max_trials):
            rp1,rp2 = self.dataset.drawSamples()
            if self.use_own_svd:
                H,status = self.dataset.findHomography4OwnSVD(rp1,rp2)
            else:
                H,status = self.dataset.findHomography4CV(rp1,rp2)
            n_in,n_out,w,score,inmask = self.dataset.computeInliers(H,self.residual_threshold)

            # if w is already 1, avoi divide by zero 
            if (np.abs(w-1) < 0.00001):
                k = i # we can stop here
            else:
                k = np.abs(np.log(1-self.stop_probability)/np.log(1-w**2)) # sometimes we get -inf
            
            # record details
            self.book[i] = {}
            self.book[i]['n_in']  = n_in
            self.book[i]['n_out'] = n_out
            self.book[i]['H']     = H
            self.book[i]['w']     = w
            self.book[i]['k']     = k
            self.book[i]['score'] = score
        
            if (self.best['n_in'] < n_in) or ( self.best['n_in'] == n_in and self.best['score'] < score):
                self.best['n_in']  = n_in
                self.best['n_out'] = n_out
                self.best['H']     = H
                self.best['iter']  = i
                self.best['score'] = score
                self.best_inmask   = inmask

            i+=1
            if (i>self.max_trials<i): break
            if (i>k): break
        # print statistics
        self.printStats()
        
    def printStats(self):
        for key in sorted(self.book.iterkeys()):
            print ("%d\t%.2f\t%d\t%d\t%.2f\t%.2f\t" % 
                   (key,
                   self.book[key]['score'],
                   self.book[key]['n_in'],
                   self.book[key]['n_out'],
                   self.book[key]['w'],
                   self.book[key]['k']))
        print (self.best)

def injectNoise(p1,p2,n_outliers):
    for i in range(n_outliers):
        xory  = np.random.randint(2)
        p1or2 = np.random.randint(2)
        j = np.random.randint(len(p1))
        if (p1or2 == 1):
            p = p2
        else:
            p = p1
        p[j,xory] = np.random.rand()*p[j,xory]


# Please refer to http://www.uio.no/studier/emner/matnat/its/UNIK4690/v16/forelesninger/lecture_4_3-estimating-homographies-from-feature-correspondences.pdf
# for information about normalization for DLT.
# We translate the centroid of the points to the origin and scale them such that the average distance of
# the points from the origin is sqrt(2)
def NormalizePointsForDLT(points):
  # We normalize the points using a matrix as below:
  # matrix = s x [1  0  -x]
  #              [0  0  -y]
  #              [0  0  -s]
  tx = np.average(points[:,0:1])
  ty = np.average(points[:,1:])

  scale = 0.0
  for i in range(0, len(points)):
    scale += math.sqrt((points[i, 0] - tx) * (points[i, 0] - tx) + (points[i, 1] - ty) * (points[i, 1] - ty))

  if scale == 0:
    scale = np.finfo(float).eps

  scale = math.sqrt(2.0) * len(points) / scale

  normalization_matrix = np.array([[scale, 0, -scale * tx], [0, scale, - scale * ty], [0, 0, 1]])

  homogeneous_points = np.append(points, np.ones([len(points), 1]), 1)

  for i in range(0, len(homogeneous_points)):
    homogeneous_points[i] = np.dot(normalization_matrix, np.transpose(homogeneous_points[i]))

  return (homogeneous_points, normalization_matrix)

# Please refer to http://6.869.csail.mit.edu/fa12/lectures/lecture13ransac/lecture13ransac.pdf
# http://vhosts.eecs.umich.edu/vision//teaching/EECS442_2011/lectures/discussion2.pdf
# http://www.cs.cmu.edu/~16385/spring15/lectures/Lecture15.pdf for information about the matrix
# A below. We try to solve an equation of the form A.H = 0 where H is the desired homography
# matrix.
def GetHomographyMatrixUsingSVD(points1, points2):
  points1_normalized, normalization_matrix1 = NormalizePointsForDLT(points1)
  points2_normalized, normalization_matrix2 = NormalizePointsForDLT(points2)

  A = []
  total_points = len(points1)

  for point_index in range(0, total_points):
    x1 = points1_normalized[point_index, 0]
    y1 = points1_normalized[point_index, 1]
    x2 = points2_normalized[point_index, 0]
    y2 = points2_normalized[point_index, 1]

    A.append([x1, y1, 1, 0, 0, 0, -x1*x2, -x2*y1, -x2])
    A.append([0, 0, 0, x1, y1, 1, -x1 * y2, -y1 * y2, -y2])

  U, D, V = np.linalg.svd(np.asarray(A), True)
  smallest_value_index = np.argmin(D)

  L = V[-1,:] / V[-1, -1]

  homography = L.reshape(3, 3)
  homography = np.dot(np.dot(np.linalg.inv(normalization_matrix2), homography), normalization_matrix1)
  return (homography, D[smallest_value_index])


#############################
# Testing begins
#############################

# create an image from file        
a = Img.imagefile('../data/list1/hill1.jpg', resize=True)
acopy = np.copy(a.image_data)
bimg  = np.copy(a.image_data)
# Translate by 10 in Y direction and 30 in X direction
bimg[10:,30:,:] = a.image_data[0:390,0:370,:]
# blank out translated pixels
bimg[0:10,:,:] = 0
bimg[:,0:30,:] = 0
# createa new image object
b  = Img(image_data=bimg,filename="b")
aa = Img(image_data=acopy,filename="aa")
a.getImageFeatures(); b.getImageFeatures(); aa.getImageFeatures();

# This should return identity matrix
p1,p2=a.matchImage(aa)
H,s=GetHomographyMatrixUsingSVD(p1,p2)

# This should return identity matrix + last column should have translation vector
p1,p2 = a.matchImage(b)
H,s=GetHomographyMatrixUsingSVD(p1,p2)
print (np.round(H))


#injectNoise(p1,p2,500)
#MP = MatchPoints(p1,p2)
#r = Ransac(MP,max_trials=1000,residual_threshold=3,use_own_svd=False)
#r.estimate()
#
#rs = Ransac(MP,max_trials=1000,residual_threshold=3,use_own_svd=True)
#rs.estimate()

MP.summarizeMethods(rs.best['H'], rs.best_inmask,0.1)
#homography, status = cv2.findHomography(p1, p2, cv2.RANSAC, 1)
#print (homography)
#print(computeInlier(p1,p2,homography))
#print(computeInlier(p1,p2,homography,t=3))


