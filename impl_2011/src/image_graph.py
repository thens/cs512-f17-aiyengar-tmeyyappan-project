# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20th 2017

@author: {aiyengar,thens}@hawk.iit.edu
"""

import numpy as np
import cv2
from operator import itemgetter

# An individual node in the image graph.
class ImageMatches:
  def __init__(self, from_index, to_index, matches):
    self.from_index = from_index
    self.to_index = to_index
    self.matches = matches

  def Print(self):
    print("Match from: ", self.from_index, " to: ", self.to_index)
    print("Matches: ", self.matches)

# This class provides functionality to build an image graph based on matching
# images on their features. This allows us to build a list of connected images
# which are then used to build panoramic images.
# TODO
# This class computes SIFT features which should be returned to avoid recomputing
# them in the caller.
class ImageGraph:
  def __init__(self, image_files):
    self.image_files = image_files
    self.image_list = [self.ReadImage(image_file) for image_file in image_files]
    self.node_list = dict()
    self.sift_descriptor = cv2.xfeatures2d.SIFT_create()
    self.ProcessImages()

  def GetImageList(self):
    if (len(self.image_files) <= 1):
      return self.image_files
    connected_components = self.GetConnectedComponents()
    reordered_image_files = []
    for i in range(0, len(connected_components)):
      reordered_image_files.append(self.image_files[connected_components[i]] )
    return reordered_image_files
      
  # Reads the |image_file| passed in.  It resizes the image to 400, 400 to ensure that
  # we don't have to deal with large images which can be extremely slow to process.
  def ReadImage(self, image_file):
    image = cv2.imread(image_file)
    if (image is None):
      print("Failed to read image file: ", image_file)
      return image

    image = cv2.resize(image, (400, 400))
    return image

  def ProcessImages(self):
    for i in range(0, len(self.image_list)):
      self.MatchImages(i)

  def MatchImages(self, image_index):
    for i in range(0, len(self.image_list)):
      if i == image_index:
        continue
      (pts_image1, pts_image2) = self.Match(self.image_list[image_index], self.image_list[i])
      if (pts_image1 is None):
        continue

      self.node_list.setdefault(image_index, []).append(ImageMatches(image_index, i, len(pts_image1)))
      print("Image Graph contains: ", len(self.node_list))
      self.PrintMatches()

  def PrintMatches(self):
    print("Length: ", len(self.node_list))
    for next in range(0, len(self.node_list)):
      if self.node_list.get(next) is None:
        continue
      for index in range(0, len(self.node_list[next])):
        self.node_list[next][index].Print()

  def GetFeaturesForImage(self, image):
    (sift_keypoints, features) = self.sift_descriptor.detectAndCompute(image, None)

    keypoints = np.float32([keypoint.pt for keypoint in sift_keypoints])
    return (keypoints, features)

  def Match(self, image1, image2, ratio=0.75):
    rows1, cols1 = image1.shape[:2]
    rows2, cols2 = image2.shape[:2]

    image1_keypoints, image1_features = self.GetFeaturesForImage(image1)
    image2_keypoints, image2_features = self.GetFeaturesForImage(image2)

    #Brute force matching with k nearest neighbors.  We take k = 2 to apply
    #ratio test. https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html

    matcher = cv2.BFMatcher()
    matches = matcher.knnMatch(image1_features, image2_features, k=2)

    # Apply ratio test
    good_features = []
    for m, n in matches:
      if m.distance < ratio * n.distance:
        good_features.append((m.trainIdx, m.queryIdx, m.distance))

    good_features = sorted(good_features, key=itemgetter(2))

    if len(good_features) > 30:
      pts_image1 = np.float32([image1_keypoints[i] for (_, i, _) in good_features])
      pts_image2 = np.float32([image2_keypoints[i] for (i, _, _) in good_features])

      return (pts_image1, pts_image2)

    return (None, None)

  def GetConnectedComponents(self):
    if (len(self.node_list) == 0):
      print("Empty image graph. No connected components")
      return []

    visited = np.zeros((len(self.image_files)))
    print("Visited length: ", visited.shape)

    connected_components_till_now = []

    for i in range(0, len(self.node_list)):
      if (visited[i] == 0):
        connected_components_for_current_node = []

        self.DFS(i, visited, connected_components_for_current_node)

        if (len(connected_components_for_current_node) > len(connected_components_till_now)):
          connected_components_till_now = connected_components_for_current_node
        
        print("Connected components for current node are: ", connected_components_for_current_node)

    print("Connected components are: ", connected_components_till_now)
    return connected_components_till_now

  def DFS(self, index, visited, components):
    visited[index] = 1

    print("Appending index to list: ", index)
    components.append(index)

    if self.node_list.get(index) is None:
      print("Skipping empty list at index: ", index)
      return
    
    for adjacent in range(0, len(self.node_list[index])):
      if (visited[self.node_list[index][adjacent].to_index] == 0):
        print("visiting adjacent node: ", self.node_list[index][adjacent].to_index)
        self.DFS(self.node_list[index][adjacent].to_index, visited, components)
