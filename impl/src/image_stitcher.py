import cv2
import numpy as np
import sys
import os
import math
from operator import itemgetter
from collections import defaultdict
from image_graph import ImageGraph

# These enums specify the algorithm to be used for computing the homography matrix.
class HomographyAlgorithm:
  OPEN_CV = 1
  PLAIN_SVD = 2
  RANSAC_SVD = 3

# This class provides functionality for image stitching. The assumption is that
# the image list is provided in a left to right order. We use SIFT to find matching
# keypoints in the images. These keypoints are used to warp the images together to
# produce a panoramic view. We use opencv for the most part.
class ImageStitcher:
  # Initialization function (ctor). Sets up member variables for sharing state
  # within an instance.
  def __init__(self):
    self.MAIN_WINDOW_TITLE = "Panorama Stitcher Main Window"
    cv2.startWindowThread()

    self.images = []
    self.left_images = []
    self.right_images = []
    self.center_image = None
    self.output_image = None
    self.image_count = 0
    self.image_center = 0

    self.ProcessCommandLine()
  
  # Reads the |image_file| passed in.  It resizes the image to 400, 400 to ensure that
  # we don't have to deal with large images which can be extremely slow to process.
  def ReadImage(self, image_file):
    image = cv2.imread(image_file)
    if (image is None):
      print("Failed to read image file: ", image_file)
      return image

    image = cv2.resize(image, (400, 400))
    return image

  # Processes the command line of the program. The initial image directory is mandatory.
  # The user can pass in other image directories by pressing the 'i' key later on. Please
  # refer to the Help() function for details.
  def ProcessCommandLine(self):
    if len(sys.argv) < 2:
      print("Insufficient arguments. The usage is image_stitcher.py <Image Directory>. Please pass the image directory\n")
      self.Exit()

    if (self.OnNewImageDirectory(sys.argv[1]) == False):
      print("Exiting")
      self.Exit()

  # Called when we have a new image directory |directory| to process. Initiates a read of the images
  # in the directory. Creates the image lists and invokes the CreatePanorama() function which combines
  # the images together and creates a panoramic view.
  def OnNewImageDirectory(self, directory):
    self.images = []
    self.image_files = []
    self.image_count = 0
    self.left_images = []
    self.right_images = []
    self.output_image = None

    try:
      self.image_files = [os.path.join(directory, file_name) for file_name in os.listdir(directory) if os.path.isfile(os.path.join(directory, file_name))]
      image_graph = ImageGraph(self.image_files)
      self.image_files = image_graph.GetImageList()
      self.images = [self.ReadImage(image_file) for image_file in self.image_files]
      self.image_count = len(self.images)
      self.SetupImageLists()

      # Add calls to CreatePanorama for every homography algorithm we support.
      #self.CreatePanorama(HomographyAlgorithm.OPEN_CV, self.MAIN_WINDOW_TITLE + " using OpenCV homography")
      self.CreatePanorama(HomographyAlgorithm.PLAIN_SVD, self.MAIN_WINDOW_TITLE + " using Plain SVD")
      return True

    except OSError as err:
      print("Invalid directory: ", directory)
      return False

  # Splits the list of images into a left and right list.
  def SetupImageLists(self):
    if (self.image_count == 0):
      print("No images found\n")
      return
    self.image_center = self.image_count // 2
    print("Number of images: ", self.image_count)
    print("Image center is: ", self.image_center)

    self.center_image = self.images[self.image_center]
    for index in range(self.image_count):
      if (index < self.image_center):
        self.left_images.append(self.images[index])
      else:
        self.right_images.append(self.images[index])

    #self.image_graph = ImageGraph(self.left_images + self.right_images)

  # Creates a panoramic view from the image lists.
  def CreatePanorama(self, homography_algorithm, window_title):
    if (self.image_count > 1):
      current = self.left_images[0]
      temp_image = current

      for next in self.left_images[1:]:
        (pts_image1, pts_image2) = self.Match(current, next)
        if (pts_image1 is None):
          print("Failed to match image. Match failed\n")
          self.Exit()

        homography, status = self.GetHomographyMatrix(pts_image1, pts_image2, homography_algorithm)
        if (homography is None):
          print("Failed to match image. GetHomographyMatrix() failed\n")
          self.Exit()

        temp_image = self.WarpSourceWithDestination(next, current, homography)
        current = temp_image

      for next in self.right_images:
        (pts_image1, pts_image2) = self.Match(current, next)
        if (pts_image1 is None):
          print("Failed to match image. Match failed\n")
          self.Exit()

        homography, status = self.GetHomographyMatrix(pts_image1, pts_image2, homography_algorithm)
        if (homography is None):
          print("Failed to match image. GetHomographyMatrix() failed\n")
          self.Exit()

        temp_image = self.WarpSourceWithDestination(next, current, homography)
        current = temp_image

      self.output_image = temp_image
    else:
      cv2.imshow(window_title, self.center_image)
      self.output_image = self.center_image
    cv2.imshow(window_title, self.output_image)
  
  # Warps the |source| image to |destination| using the |homography| matrix
  # passed in.
  def WarpSourceWithDestination(self, destination, source, homography):
    rows_destination, cols_destination = destination.shape[:2]
    rows_source, cols_source = source.shape[:2]

    destination_points = np.float32([[0, 0], [0, rows_destination],
        [cols_destination, rows_destination], [cols_destination, 0]]).reshape(-1, 1, 2)

    source_points = np.float32([[0, 0], [0, rows_source], [cols_source, rows_source],
        [cols_source, 0]]).reshape(-1, 1, 2)

    # Find the perspective transformation for the query image (image2), which will
    # be used to transform the corners of the image to corresponding points in the
    # training image (image1)
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html
    transformed_source_points = cv2.perspectiveTransform(source_points, homography)
    merged_points = np.concatenate((destination_points, transformed_source_points), axis=0)

    # Assuming that starting point of the image is (0, 0) and the ending point
    # is (end_row, end_col), the warped image starts from homography x [0, 0]
    # to homography x [end_row, end_col]. If the start point is -ve we need to
    # translate start by homography x [0, 0].

    # Compute the translation which needs to be applied to image2.
    [x_min, y_min] = np.int32(merged_points.min(axis=0).ravel() - 0.5)
    [x_max, y_max] = np.int32(merged_points.max(axis=0).ravel() + 0.5)

    # Apply the translation to the homography matrix
    translation_dist = [-x_min, -y_min]
    h_translation = np.array([[1, 0, translation_dist[0]], [0, 1, translation_dist[1]], [0, 0, 1]])

    result = cv2.warpPerspective(source, h_translation.dot(homography), (x_max - x_min, y_max - y_min))
    return self.BlendImageWithTarget(result, destination, (translation_dist[0], translation_dist[1]), 0.05)

  # Matches |image1| and |image2| using the opencv SIFT descriptor and returns the matching
  # keypoints on success. On failure returns empty keypoints.
  def Match(self, image1, image2, ratio=0.75):
    rows1, cols1 = image1.shape[:2]
    rows2, cols2 = image2.shape[:2]

    image1_keypoints, image1_features = self.GetFeaturesForImage(image1)
    image2_keypoints, image2_features = self.GetFeaturesForImage(image2)

    #Brute force matching with k nearest neighbors.  We take k = 2 to apply
    #ratio test. https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html

    matcher = cv2.BFMatcher()
    matches = matcher.knnMatch(image1_features, image2_features, k=2)

    # Apply ratio test
    good_features = []
    for m, n in matches:
      if m.distance < ratio * n.distance:
        good_features.append((m.trainIdx, m.queryIdx, m.distance))

    good_features = sorted(good_features, key=itemgetter(2))

    if len(good_features) > 4:
      pts_image1 = np.float32([image1_keypoints[i] for (_, i, _) in good_features])
      pts_image2 = np.float32([image2_keypoints[i] for (i, _, _) in good_features])

      return (pts_image1, pts_image2)

    return (None, None)

  # Returns features for the |image| using the opencv SIFT implementation.
  def GetFeaturesForImage(self, image):
    sift_descriptor = cv2.xfeatures2d.SIFT_create()
    (sift_keypoints, features) = sift_descriptor.detectAndCompute(image, None)

    keypoints = np.float32([keypoint.pt for keypoint in sift_keypoints])
    return (keypoints, features)

  # Exits the program.
  def Exit(self):
    cv2.destroyAllWindows()
    sys.exit()

  # Returns the homography matrix for the points passed in. The matrix allows
  # for transforming |points1| into the space for |points2|. Currently uses the opencv
  # function to return the matrix. We will provide our own implementations for the
  # homography matrix here.
  def GetHomographyMatrix(self, points1, points2, homography_algorithm):
    if (homography_algorithm == HomographyAlgorithm.OPEN_CV):
      homography, status = cv2.findHomography(points1, points2, cv2.RANSAC, 3)
      print("\n OpenCV homography matrix is")
      print(homography)
      return (homography, status)
    elif (homography_algorithm == HomographyAlgorithm.PLAIN_SVD):
      # Best effort to find a valid homography. We use a sliding window approach to find
      # the best homography. We assume a homography with a singular value smaller than 0.5
      # as a good one. Singular values greater than that give very bad results.
      sample_size = len(points1) / 5
      for i in range(0, len(points1) - sample_size):
        points1_slice = points1[i:i+sample_size]
        points2_slice = points2[i:i+sample_size]
        homography, singular_value = self.GetHomographyMatrixUsingSVD(points1_slice, points2_slice)
        if np.isnan(singular_value):
          continue
        if (singular_value <= 0.5):
          print("Singular value: ", singular_value)
          print("\n Plain SVD homography matrix")
          print(homography)
          return (homography, 1)
        else:
          print("Continuing search for better homography matrix. Current Singular value: ", singular_value)
    else:
      print("Unsupported homography algorithm: ", homography_algorithm)
    return (None, -1)
  
  # Please refer to http://www.uio.no/studier/emner/matnat/its/UNIK4690/v16/forelesninger/lecture_4_3-estimating-homographies-from-feature-correspondences.pdf
  # for information about normalization for DLT.
  # We translate the centroid of the points to the origin and scale them such that the average distance of
  # the points from the origin is sqrt(2)
  def NormalizePointsForDLT(self, points):
    # We normalize the points using a matrix as below:
    # matrix = s x [1  0  -x]
    #              [0  0  -y]
    #              [0  0  -s]
    tx = np.average(points[:,0:1])
    ty = np.average(points[:,1:])

    scale = 0.0
    for i in range(0, len(points)):
      scale += math.sqrt((points[i, 0] - tx) * (points[i, 0] - tx) + (points[i, 1] - ty) * (points[i, 1] - ty))

    if scale == 0:
      scale = np.finfo(float).eps

    scale = math.sqrt(2.0) * len(points) / scale

    normalization_matrix = np.array([[scale, 0, -scale * tx], [0, scale, - scale * ty], [0, 0, 1]])

    homogeneous_points = np.append(points, np.ones([len(points), 1]), 1)

    for i in range(0, len(homogeneous_points)):
      homogeneous_points[i] = np.dot(normalization_matrix, np.transpose(homogeneous_points[i]))

    return (homogeneous_points, normalization_matrix)

  # Please refer to http://6.869.csail.mit.edu/fa12/lectures/lecture13ransac/lecture13ransac.pdf
  # http://vhosts.eecs.umich.edu/vision//teaching/EECS442_2011/lectures/discussion2.pdf
  # http://www.cs.cmu.edu/~16385/spring15/lectures/Lecture15.pdf for information about the matrix
  # A below. We try to solve an equation of the form A.H = 0 where H is the desired homography
  # matrix.
  def GetHomographyMatrixUsingSVD(self, points1, points2):
    points1_normalized, normalization_matrix1 = self.NormalizePointsForDLT(points1)
    points2_normalized, normalization_matrix2 = self.NormalizePointsForDLT(points2)

    A = []
    total_points = len(points1)

    for point_index in range(0, total_points):
      x1 = points1_normalized[point_index, 0]
      y1 = points1_normalized[point_index, 1]
      x2 = points2_normalized[point_index, 0]
      y2 = points2_normalized[point_index, 1]

      A.append([x1, y1, 1, 0, 0, 0, -x1*x2, -x2*y1, -x2])
      A.append([0, 0, 0, x1, y1, 1, -x1 * y2, -y1 * y2, -y2])

    U, D, V = np.linalg.svd(np.asarray(A), True)
    smallest_value_index = np.argmin(D)

    L = V[-1,:] / V[-1, -1]

    homography = L.reshape(3, 3)
    homography = np.dot(np.dot(np.linalg.inv(normalization_matrix2), homography), normalization_matrix1)
    return (homography, D[smallest_value_index])

  def BlendImageWithTarget(self, target, image, start_point, blend_ratio = 0.1):
    x_start = start_point[0]
    y_start = start_point[1]

    blended_image = np.copy(target)

    # Generate a mask with the same size as the target. The values in the mask
    # are dependent on the proximity of the pixels to the |image|, which is being
    # blended with the target.
    mask = np.zeros((target.shape[0], target.shape[1]), 'int')

    x_bounds = x_start + image.shape[1]
    y_bounds = y_start + image.shape[0]

    x_bounds = min(x_bounds, mask.shape[1])
    y_bounds = min(y_bounds, mask.shape[0])

    blend_window_width = blend_ratio * image.shape[1]

    for i in range (y_start, y_bounds):
      for j in range (x_start, x_bounds):
        # Find the nearest edge.
        closest_edge = j - x_start + 1
        # The intensity is higher, the closer the edge is. We can think of this
        # like a probability value.
        intensity = min(1.0, closest_edge / blend_window_width)
        # Normalize to range between 0 and 255.
        intensity = intensity * 255
        mask[i, j] = intensity

    for i in range (0, mask.shape[0]):
      for j in range (0, mask.shape[1]):
        # Black = target.
        if (mask[i, j] == 0):
          blended_image[i, j] = target[i, j]
        # Black target = copy from image.
        elif (mask[i, j] > 0 and target[i, j][0] <= 10 and target[i, j][1] <= 10 and target[i, j][2] <= 10):
          blended_image[i, j] = image[i - start_point[1], j - start_point[0]]
        else:
          # We have an overlap with the image and the target. The final pixel value is dependent on whether it
          # is closer to the target(warped image) or to the |image| which needs to be blended.
          blended_image[i, j] = mask[i, j] / 255.0 * image[i - start_point[1], j - start_point[0]] + (1 - mask[i, j] / 255.0) * target[i,j]

    return blended_image

  # Displays help for the program. Invoked via the 'h' key.
  def Help(self):
    print("\nPanorama Stitching for Static images\n")
    print("Usage: python image_stitcher.py <image directory>\n")
    print("Press ESC to exit")
    print("Press h for Help")
    print("Press i to enter a new image directory")

  # Invoked when the user wants to enter a new image directory for producing a panoramic
  # view.
  def GetImageDirectory(self):
    directory = raw_input('\nEnter image directory: ')
    self.OnNewImageDirectory(directory)

  # Processes the command identified by |key|. Supported commands today are ESC, 'h' and 'i'
  def ProcessCommand(self, key):
    if key == 27:
      self.Exit()
    elif key == ord('h'):
      self.Help()
    elif key == ord('i'):
      self.GetImageDirectory()

  # Workhorse loop for processing user input.
  def RunLoop(self):
    while (True):
      key = cv2.waitKey(0)
      self.ProcessCommand(key)

image_stitcher = ImageStitcher()
image_stitcher.RunLoop()