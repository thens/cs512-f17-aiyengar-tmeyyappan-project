# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 12:37:54 2017

@author: {aiyengar,thens}@hawk.iit.edu
"""
import sys
from PanoramaConfig import PanoramaConfig

from Panorama import Panorama
from Img import Img

def process_commandline():
    if len(sys.argv) < 2:
        print("Insufficient arguments. The usage is image_stitcher.py <Image Directory>. Please pass the image directory\n")
        sys.exit()
    pan = Panorama()
    pan.OnNewImageDirectory(sys.argv[1], pc.get_option('homography', 'algorithm'))
    pan.RunLoop()

pc = PanoramaConfig("panorama.ini")
process_commandline()
